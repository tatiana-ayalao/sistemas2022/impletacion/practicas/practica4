﻿DROP DATABASE IF EXISTS practica4mod1; 
CREATE DATABASE practica4mod1;
USE practica4mod1;

CREATE TABLE empleado (
nssempleado varchar (100),
nombre varchar (100),
apellido varchar (100),
iniciales varchar (100),
fechaNcto date,
sexo varchar (100),
direccion varchar (100),
salario int,
nombreDPertenece varchar (100),
numeroDptoPertenece int,
  PRIMARY KEY (nssempleado)
);

CREATE TABLE departamento (
nombreD varchar (100),
numeroD int,
numDEmpleados int,
nssempleadoDirige varchar (100),
fechaInicioJefe date,
PRIMARY KEY (nombreD, numeroD ),
  UNIQUE KEY (nssempleadoDirige)
   );

CREATE TABLE supervisa (
nssEmpleado varchar (100),
nssSupervisor varchar (100),
PRIMARY KEY (nssEmpleado, nssSupervisor),
UNIQUE KEY (nssEmpleado)

);

CREATE TABLE dependiente (
nombreDependiente varchar (100),
nssempleado varchar (100),
sexo varchar (100),
fechaNcto date,
parentesco varchar (100),
PRIMARY KEY (nombreDependiente, nssempleado)
  );


CREATE TABLE localizaciones (
nombreD varchar (100),
numeroD int,
LocalizacionDept varchar (100),
PRIMARY KEY (nombreD, numeroD, LocalizacionDept)
 
);

CREATE TABLE proyecto (
numeroP int,
nombreP varchar (100),
localizacion varchar (100),
nombreDControla varchar (100),
numeroDcontrola int,
PRIMARY KEY (nombreP, numeroP)
);

CREATE TABLE trabajaEn (
nssEmpleado varchar (100),
nombreP varchar (100),
NumeroP int,
hora datetime,
PRIMARY KEY (nssEmpleado, nombreP, NumeroP)

);

ALTER TABLE empleado 
  ADD CONSTRAINT fkempleadoDepartamento FOREIGN KEY (nombreDPertenece, numeroDptoPertenece) REFERENCES departamento (nombreD, numeroD) ON DELETE RESTRICT ON UPDATE RESTRICT
  ;

ALTER TABLE departamento
  ADD CONSTRAINT fkdepartamentoEmpleado FOREIGN KEY (nssempleadoDirige) REFERENCES empleado (nssempleado) ON DELETE RESTRICT ON UPDATE RESTRICT
  ;

ALTER TABLE supervisa
  ADD CONSTRAINT fksupervisaEmpleado1 FOREIGN KEY (nssEmpleado) REFERENCES empleado (nssempleado) ON DELETE RESTRICT ON UPDATE RESTRICT,
  ADD CONSTRAINT fksupervisaEmpleado2 FOREIGN KEY (nssSupervisor) REFERENCES empleado (nssempleado) ON DELETE RESTRICT ON UPDATE RESTRICT
  ;

ALTER TABLE dependiente
  ADD CONSTRAINT fkDependienteEmpleado FOREIGN KEY (nssempleado) REFERENCES empleado (nssempleado) ON DELETE RESTRICT ON UPDATE RESTRICT
 ;

ALTER TABLE localizaciones 
  ADD CONSTRAINT fklocalizacionesDepartamento FOREIGN KEY (nombreD, numeroD) REFERENCES departamento (nombreD, numeroD) ON DELETE RESTRICT ON UPDATE RESTRICT
  ;

ALTER TABLE proyecto 
  ADD CONSTRAINT fkproyectoDepartamento FOREIGN KEY (nombreDControla, numeroDcontrola) REFERENCES departamento (nombreD, numeroD) ON DELETE RESTRICT ON UPDATE RESTRICT
  ; 

ALTER TABLE trabajaEn
  ADD CONSTRAINT fktrabajaenEmpleado FOREIGN KEY (nssEmpleado) REFERENCES empleado (nssempleado) ON DELETE RESTRICT ON UPDATE RESTRICT,
  ADD CONSTRAINT fktrabajaenProyecto FOREIGN KEY (nombreP, NumeroP) REFERENCES proyecto (nombreP, numeroP) ON DELETE RESTRICT ON UPDATE RESTRICT
  ;






