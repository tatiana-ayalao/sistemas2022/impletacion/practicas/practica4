﻿--
-- Script was generated by Devart dbForge Studio 2019 for MySQL, Version 8.2.23.0
-- Product home page: http://www.devart.com/dbforge/mysql/studio
-- Script date 13/09/2022 13:57:38
-- Server version: 5.7.33
-- Client version: 4.1
--

-- 
-- Disable foreign keys
-- 
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;

-- 
-- Set SQL mode
-- 
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- 
-- Set character set the client will use to send SQL statements to the server
--
SET NAMES 'utf8';

DROP DATABASE IF EXISTS practica4mod1;

CREATE DATABASE IF NOT EXISTS practica4mod1
CHARACTER SET latin1
COLLATE latin1_swedish_ci;

--
-- Set default database
--
USE practica4mod1;

--
-- Create table `departamento`
--
CREATE TABLE IF NOT EXISTS departamento (
  nombreD varchar(100) NOT NULL,
  numeroD int(11) NOT NULL,
  numDEmpleados int(11) DEFAULT NULL,
  nssempleadoDirige varchar(100) DEFAULT NULL,
  fechaInicioJefe date DEFAULT NULL,
  PRIMARY KEY (nombreD, numeroD)
)
ENGINE = INNODB,
AVG_ROW_LENGTH = 5461,
CHARACTER SET latin1,
COLLATE latin1_swedish_ci;

--
-- Create index `nssempleadoDirige` on table `departamento`
--
ALTER TABLE departamento
ADD UNIQUE INDEX nssempleadoDirige (nssempleadoDirige);

--
-- Create table `proyecto`
--
CREATE TABLE IF NOT EXISTS proyecto (
  numeroP int(11) NOT NULL,
  nombreP varchar(100) NOT NULL,
  localizacion varchar(100) DEFAULT NULL,
  nombreDControla varchar(100) DEFAULT NULL,
  numeroDcontrola int(11) DEFAULT NULL,
  PRIMARY KEY (nombreP, numeroP)
)
ENGINE = INNODB,
AVG_ROW_LENGTH = 5461,
CHARACTER SET latin1,
COLLATE latin1_swedish_ci;

--
-- Create foreign key
--
ALTER TABLE proyecto
ADD CONSTRAINT fkproyectoDepartamento FOREIGN KEY (nombreDControla, numeroDcontrola)
REFERENCES departamento (nombreD, numeroD);

--
-- Create table `localizaciones`
--
CREATE TABLE IF NOT EXISTS localizaciones (
  nombreD varchar(100) NOT NULL,
  numeroD int(11) NOT NULL,
  LocalizacionDept varchar(100) NOT NULL,
  PRIMARY KEY (nombreD, numeroD, LocalizacionDept)
)
ENGINE = INNODB,
AVG_ROW_LENGTH = 8192,
CHARACTER SET latin1,
COLLATE latin1_swedish_ci;

--
-- Create foreign key
--
ALTER TABLE localizaciones
ADD CONSTRAINT fklocalizacionesDepartamento FOREIGN KEY (nombreD, numeroD)
REFERENCES departamento (nombreD, numeroD);

--
-- Create table `empleado`
--
CREATE TABLE IF NOT EXISTS empleado (
  nssempleado varchar(100) NOT NULL,
  nombre varchar(100) DEFAULT NULL,
  apellido varchar(100) DEFAULT NULL,
  iniciales varchar(100) DEFAULT NULL,
  fechaNcto date DEFAULT NULL,
  sexo varchar(100) DEFAULT NULL,
  direccion varchar(100) DEFAULT NULL,
  salario int(11) DEFAULT NULL,
  nombreDPertenece varchar(100) DEFAULT NULL,
  numeroDptoPertenece int(11) DEFAULT NULL,
  PRIMARY KEY (nssempleado)
)
ENGINE = INNODB,
AVG_ROW_LENGTH = 2730,
CHARACTER SET latin1,
COLLATE latin1_swedish_ci;

--
-- Create foreign key
--
ALTER TABLE empleado
ADD CONSTRAINT fkempleadoDepartamento FOREIGN KEY (nombreDPertenece, numeroDptoPertenece)
REFERENCES departamento (nombreD, numeroD);

--
-- Create foreign key
--
ALTER TABLE departamento
ADD CONSTRAINT fkdepartamentoEmpleado FOREIGN KEY (nssempleadoDirige)
REFERENCES empleado (nssempleado);

--
-- Create table `trabajaen`
--
CREATE TABLE IF NOT EXISTS trabajaen (
  nssEmpleado varchar(100) NOT NULL,
  nombreP varchar(100) NOT NULL,
  NumeroP int(11) NOT NULL,
  hora datetime DEFAULT NULL,
  PRIMARY KEY (nssEmpleado, nombreP, NumeroP)
)
ENGINE = INNODB,
CHARACTER SET latin1,
COLLATE latin1_swedish_ci;

--
-- Create foreign key
--
ALTER TABLE trabajaen
ADD CONSTRAINT fktrabajaenEmpleado FOREIGN KEY (nssEmpleado)
REFERENCES empleado (nssempleado);

--
-- Create foreign key
--
ALTER TABLE trabajaen
ADD CONSTRAINT fktrabajaenProyecto FOREIGN KEY (nombreP, NumeroP)
REFERENCES proyecto (nombreP, numeroP);

--
-- Create table `supervisa`
--
CREATE TABLE IF NOT EXISTS supervisa (
  nssEmpleado varchar(100) NOT NULL,
  nssSupervisor varchar(100) NOT NULL,
  PRIMARY KEY (nssEmpleado, nssSupervisor)
)
ENGINE = INNODB,
AVG_ROW_LENGTH = 4096,
CHARACTER SET latin1,
COLLATE latin1_swedish_ci;

--
-- Create index `nssEmpleado` on table `supervisa`
--
ALTER TABLE supervisa
ADD UNIQUE INDEX nssEmpleado (nssEmpleado);

--
-- Create foreign key
--
ALTER TABLE supervisa
ADD CONSTRAINT fksupervisaEmpleado1 FOREIGN KEY (nssEmpleado)
REFERENCES empleado (nssempleado);

--
-- Create foreign key
--
ALTER TABLE supervisa
ADD CONSTRAINT fksupervisaEmpleado2 FOREIGN KEY (nssSupervisor)
REFERENCES empleado (nssempleado);

--
-- Create table `dependiente`
--
CREATE TABLE IF NOT EXISTS dependiente (
  nombreDependiente varchar(100) NOT NULL,
  nssempleado varchar(100) NOT NULL,
  sexo varchar(100) DEFAULT NULL,
  fechaNcto date DEFAULT NULL,
  parentesco varchar(100) DEFAULT NULL,
  PRIMARY KEY (nombreDependiente, nssempleado)
)
ENGINE = INNODB,
AVG_ROW_LENGTH = 8192,
CHARACTER SET latin1,
COLLATE latin1_swedish_ci;

--
-- Create foreign key
--
ALTER TABLE dependiente
ADD CONSTRAINT fkDependienteEmpleado FOREIGN KEY (nssempleado)
REFERENCES empleado (nssempleado);

-- 
-- Dumping data for table departamento
--
INSERT INTO departamento VALUES
('contabilidad', 1, 0, '105', '2022-09-13'),
('Contabilidad', 2, 1, '101', '2003-04-15'),
('Ventas', 1, 1, '100', '2022-09-13');

-- 
-- Dumping data for table proyecto
--
INSERT INTO proyecto VALUES
(2, 'Centro de Costos', 'Santander', 'Contabilidad', 2),
(3, 'Gestion residuos La Maruca', 'Santander', 'ventas', 1),
(1, 'Gestion residuos sardinero', 'Santander', 'ventas', 1);

-- 
-- Dumping data for table empleado
--
INSERT INTO empleado VALUES
('100', 'Marcos', 'Castañedo', 'MC', '1973-01-12', 'M', NULL, 1000, NULL, NULL),
('101', 'Martin', 'Caballero', 'MA', '1973-01-31', 'M', NULL, 1000, NULL, NULL),
('102', 'Maria Jesus', 'Sanchez', 'MS', '1984-09-30', 'F', NULL, 990, 'Contabilidad', 2),
('103', 'Jorge', 'Arce', 'JA', NULL, 'M', NULL, 1100, 'Ventas', 1),
('104', 'Luisa', 'Gomez', 'LG', NULL, 'F', NULL, 991, 'Ventas', 1),
('105', 'Roberto', 'lopez', 'RL', NULL, 'M', NULL, 2000, NULL, NULL);

-- 
-- Dumping data for table trabajaen
--
-- Table practica4mod1.trabajaen does not contain any data (it is empty)

-- 
-- Dumping data for table supervisa
--
INSERT INTO supervisa VALUES
('101', '105'),
('102', '105'),
('103', '100'),
('104', '100');

-- 
-- Dumping data for table localizaciones
--
INSERT INTO localizaciones VALUES
('Contabilidad', 2, 'Santander'),
('Ventas', 1, 'Madrid');

-- 
-- Dumping data for table dependiente
--
INSERT INTO dependiente VALUES
('Ana Isabel', '104', 'F', '2001-12-31', 'madre'),
('Jorge Angel', '104', 'M', '1980-10-20', 'hijo');

-- 
-- Restore previous SQL mode
-- 
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;

-- 
-- Enable foreign keys
-- 
/*!40014 SET FOREIGN_KEY_CHECKS = @OLD_FOREIGN_KEY_CHECKS */;