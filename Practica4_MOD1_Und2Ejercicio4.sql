﻿USE practica4mod1;

INSERT INTO empleado 
  (nssempleado, nombre, apellido, iniciales, fechaNcto, sexo, direccion, salario, nombreDPertenece,numeroDptoPertenece) VALUES 
  ('100', 'Marcos', 'Castañedo', 'MC',       '1973/01/12', 'M', NULL, 1000, NULL,NULL),
  ('101', 'Martin', 'Caballero', 'MA',       '1973/01/31', 'M', NULL, 1000, NULL,NULL);
  

INSERT INTO departamento 
  (nombreD, numeroD, numDEmpleados, nssempleadoDirige, fechaInicioJefe) VALUES 
  ('Ventas',       1,       1,       '100', CURDATE()),
  ('Contabilidad', 2,       1,     '101', '2003/04/15');

INSERT INTO empleado (nssempleado, nombre, apellido, iniciales, fechaNcto, sexo, direccion, salario, nombreDPertenece,numeroDptoPertenece) VALUES 
                        ('102', 'Maria Jesus', 'Sanchez','MS', '1984/09/30', 'F', NULL, 990,'Contabilidad', 2 );


SELECT * FROM empleado e;
SELECT * FROM departamento d;

INSERT INTO empleado 
  (nssempleado, nombre, apellido, iniciales, fechaNcto, sexo, direccion, salario, nombreDPertenece, numeroDptoPertenece) VALUES 
  ('103',      'Jorge', 'Arce',   'JA',      NULL,      'M',   NULL ,    1100,       'Ventas',    1),                
  ('104',      'Luisa', 'Gomez',  'LG',      NULL,      'F',   NULL,      991,       'Ventas', 1),
  ('105',     'Roberto','lopez',  'RL',      NULL,      'M',   NULL,     2000,        NULL,   NULL);


SELECT * FROM empleado e;

INSERT INTO departamento (nombreD, numeroD, numDEmpleados, nssempleadoDirige, fechaInicioJefe)
  VALUES ('contabilidad', 1, 0, '105', CURDATE());

   SELECT * FROM departamento d;

INSERT INTO supervisa 
  (nssEmpleado, nssSupervisor) VALUES 

  ('101',        '105'),
  ('102',        '105'),
  ('103',        '100'),
  ('104',        '100');

SELECT * FROM supervisa s;

INSERT INTO dependiente
  (nombreDependiente, nssempleado, sexo, fechaNcto, parentesco) VALUES
  ('Jorge Angel',      '104',          'M', '1980/10/20', 'hijo'),
  ('Ana Isabel' ,      '104',          'F', '2001/12/31', 'madre'); 


SELECT * FROM dependiente d;

INSERT INTO localizaciones 
  (nombreD, numeroD, LocalizacionDept) VALUES
 ('Ventas', 1, 'Madrid'),
 ('Contabilidad', 2, 'Santander');

SELECT * FROM localizaciones l;

INSERT INTO proyecto 
  (numeroP, nombreP, localizacion, nombreDControla, numeroDcontrola) VALUES 
  (1,'Gestion residuos sardinero', 'Santander',  'ventas',       1),
  (2,'Centro de Costos',           'Santander',  'Contabilidad', 2),
  (3,'Gestion residuos La Maruca', 'Santander', 'ventas',       1);

SELECT * FROM proyecto p;

INSERT INTO trabajaen 
  (nssEmpleado, nombreP, NumeroP, hora) VALUES
  ('101','Gestion residuos sardinero', 1, 10),
  ('102','Centro de Costos',           2, 5),
  ('102','Gestion residuos La Maruca'  3, 5);